import React, { Fragment, useState } from "react";
import { styled, themes, convert } from "@storybook/theming";
import { Icons } from "@storybook/components";

const Button = styled.button({
  border: 0,
  padding: "0.5em 2em",
});

const ListWrapper = styled.ul({
  listStyle: "none",
  fontSize: 14,
  padding: 0,
  margin: 0,
  height: "100%",
  position: "relative",
});

const Wrapper = styled.div({
  display: "flex",
  width: "100%",
  borderBottom: `1px solid ${convert(themes.normal).appBorderColor}`,
  "&:hover": {
    background: convert(themes.normal).background.hoverable,
  },
});

const Icon = styled(Icons)({
  height: 10,
  width: 10,
  minWidth: 10,
  color: convert(themes.normal).color.mediumdark,
  marginRight: 10,
  transition: "transform 0.1s ease-in-out",
  alignSelf: "center",
  display: "inline-flex",
});

const HeaderBar = styled.div({
  padding: convert(themes.normal).layoutMargin,
  paddingLeft: convert(themes.normal).layoutMargin - 3,
  background: "none",
  color: "inherit",
  textAlign: "left",
  cursor: "pointer",
  borderLeft: "3px solid transparent",
  width: "100%",
  display: "flex",

  "&:focus": {
    outline: "0 none",
    borderLeft: `3px solid ${convert(themes.normal).color.secondary}`,
  },
});

const Description = styled.div({
  padding: "1em 2em",
  marginBottom: convert(themes.normal).layoutMargin,
  fontStyle: "italic",
});

const Title = styled.div({
  flex: 1,
  fontWeight: "700",
});

export const ListItem = ({ item }) => {
  const [open, onToggle] = useState(false);

  return (
    <Fragment>
      <Wrapper>
        <HeaderBar onClick={() => onToggle(!open)} role="button">
          <Icon
            icon="chevrondown"
            size={10}
            color={convert(themes.normal).appBorderColor}
            style={{
              transform: `rotate(${open ? 0 : -90}deg)`,
            }}
          />
          <Title>{item.args.slice(2, item.args.length).join(" - ")}</Title>
          <div>{item.date.toISOString()}</div>
        </HeaderBar>
      </Wrapper>
      {open ? (
        <Description>
          <code>{item.description}</code>
        </Description>
      ) : null}
    </Fragment>
  );
};

const FlexColumn = styled.div({
  display: "flex",
  flexDirection: "column",
});

export const List = ({ items, clearData }) => (
  <FlexColumn>
    <Button onClick={clearData} disabled={!items.length}>
      Clear
    </Button>
    <ListWrapper>
      {items.map((item, idx) => (
        <ListItem key={idx} item={item} />
      ))}
    </ListWrapper>
  </FlexColumn>
);
