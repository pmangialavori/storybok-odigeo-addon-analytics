import React, { Fragment } from "react";
import { styled, themes, convert } from "@storybook/theming";
import { TabsState, Placeholder, Button } from "@storybook/components";
import { List } from "./List";

/**
 * Checkout https://github.com/storybookjs/storybook/blob/next/addons/jest/src/components/Panel.tsx
 * for a real world example
 */
export const PanelContent = ({ events, clearData }) => (
  <TabsState
    initial="events"
    backgroundColor={convert(themes.normal).background.hoverable}
    height="100%"
  >
    <div
      id="overview"
      title="Overview"
      color={convert(themes.normal).color.secondary}
    >
      <Placeholder>
        <Fragment>
          This addon is intended to collect analytics calls. For now only Google
          Analytics call but can be extended to handle different analytics
          providers
        </Fragment>
      </Placeholder>
    </div>
    <div
      id="events"
      title={`${events.length} Google Analytics call`}
      color={convert(themes.normal).color.positive}
    >
      <Fragment>
        <List items={events} clearData={clearData} />
      </Fragment>
    </div>
  </TabsState>
);
