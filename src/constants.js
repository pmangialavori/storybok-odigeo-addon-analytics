export const ADDON_ID = "analyticsAddon";
export const PANEL_ID = `${ADDON_ID}/panel`;

export const EVENTS = {
  RESULT: `${ADDON_ID}/result`,
  CLEAR: `${ADDON_ID}/clear`,
};
