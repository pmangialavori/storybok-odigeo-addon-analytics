import React from "react";
import { useAddonState, useChannel } from "@storybook/api";
import { STORY_CHANGED } from "@storybook/core-events";
import { AddonPanel } from "@storybook/components";
import { ADDON_ID, EVENTS } from "../constants";
import { PanelContent } from "../components/PanelContent";

export const Panel = (props) => {
  // https://storybook.js.org/docs/react/addons/addons-api#useaddonstate
  const [events, setState] = useAddonState(ADDON_ID, []);

  // https://storybook.js.org/docs/react/addons/addons-api#usechannel
  const emit = useChannel({
    [EVENTS.RESULT]: (newResults) => {
      setState((state) => {
        if (!newResults) return state;
        return [...state, ...newResults];
      });
    },
    [STORY_CHANGED]: () => {
      setState([]);
    },
    [EVENTS.CLEAR]: () => {
      setState([]);
    },
  });

  const clearData = () => emit(EVENTS.CLEAR);

  return (
    <AddonPanel {...props}>
      <PanelContent events={events} clearData={clearData} />
    </AddonPanel>
  );
};
