import { addons } from "@storybook/addons";
import { EVENTS } from "./constants";

function withChannel(fn) {
  const channel = addons.getChannel();
  return function (...args) {
    channel.emit(EVENTS.RESULT, [
      {
        args,
        date: new Date(),
        title: JSON.stringify(args),
        description: JSON.stringify(args),
      },
    ]);
    return fn.apply(window, args);
  };
}

export function initialize() {
  const channel = addons.getChannel();
  window.ga = withChannel(window.ga);
}
